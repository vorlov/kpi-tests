from pages import login
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import unittest
import os


class TestLoginPage(unittest.TestCase):
    def setUp(self) -> None:
        if path := os.getenv('CHROME_DRIVER'):
            _driver = webdriver.Chrome(executable_path=path)
        elif web := os.getenv('REMOTE_CHROME_DRIVER'):
            _driver = webdriver.Remote(command_executor=web, desired_capabilities=DesiredCapabilities.CHROME)
        else:
            raise Exception('CHROME_DRIVER or REMOTE_CHROME_DRIVER is not provided')
        self.page = login.LoginPage(_driver, 'https://web.telegram.org/')
        self.page.go_to_site()

    def tearDown(self) -> None:
        self.page.driver.close()

    def test_telegram_nacl(self):
        nacl = self.page.get_nacl_listener()

        self.assertTrue(nacl)

    def test_telegram_country_code(self):
        self.page.pick_country('Ukraine')
        code = self.page.get_country_phone_code_field().get_attribute('value')

        self.assertEqual(code, '+380')

    def test_negative_next(self):
        self.page.pick_country('Ukraine')
        self.page.click_next()
        element = self.page.get_phone_input_field()
        class_list = element.get_attribute('class').split(' ')

        self.assertIn('ng-invalid', class_list)
        self.assertIn('ng-invalid-required', class_list)

    def test_desktop_link(self):
        link_element = self.page.get_desktop_header_link()
        # :D Test Failed
        self.assertEqual(link_element.get_attribute('href'), 'https://desktop.telegram.org/')

    def test_negative_country_code(self):
        field = self.page.get_country_phone_code_field()
        field.click()
        field.clear()
        field.send_keys('+000')
        self.page.click_next()

        code_block = self.page.find_element(
            (By.CSS_SELECTOR, '.login_phone_code_input_group.md-input-error')
        )
        class_list = code_block.get_attribute('class').split(' ')

        self.assertIn('md-input-error', class_list)


if __name__ == '__main__':
    unittest.main()

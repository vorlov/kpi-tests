import typing

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

_Locator = typing.Tuple[str, str]


class BaseMixin:
    @staticmethod
    def _find_element(locator: _Locator, driver: WebDriver, time=10) -> WebElement:
        return WebDriverWait(driver, time).until(ec.presence_of_element_located(locator),
                                                 message=f"Can't find element by locator {locator}")

    @staticmethod
    def _go_to_site(url: str, driver: WebDriver):
        return driver.get(url)

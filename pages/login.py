from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from .mixins import BaseMixin, _Locator

from selenium.webdriver.common.by import By


class SearchLocators:
    COUNTRY_BUTTON = (By.XPATH, '//*[@id="ng-app"]/body/div[1]/div/div[3]/div[2]/form/div[1]/div')
    COUNTRY_FIELD = (By.XPATH, '//*[@id="ng-app"]/body/div[5]/div[2]/div/div/div[2]/div[1]/input')
    COUNTRY_CHOOSE = (By.CLASS_NAME, 'countries_modal_country')
    COUNTRY_PHONE_CODE = (By.XPATH, '//*[@id="ng-app"]/body/div[1]/div/div[3]/div[2]/form/div[2]/div[1]/input')

    HEADER = (By.XPATH, '//*[@id="ng-app"]/body/div[1]/div/div[1]/p/my-i18n-param/a')
    NEXT = (By.XPATH, '//a[@class="login_head_submit_btn"]')
    PHONE_INPUT_FIELD = (By.XPATH, '//input[@ng-model="credentials.phone_number"]')

    DESKTOP_LINK_TEXT = (By.LINK_TEXT, 'desktop.telegram.org')
    NACL_LISTENER = (By.ID, 'nacl_listener')


class LoginPage(BaseMixin):
    def __init__(self, driver: WebDriver, link: str):
        self.driver = driver
        self.link = link

    def pick_country(self, country: str):
        self._find_element(
            locator=SearchLocators.COUNTRY_BUTTON,
            driver=self.driver
        ).click()

        field = self._find_element(
            locator=SearchLocators.COUNTRY_FIELD,
            driver=self.driver
        )

        field.click()
        field.send_keys(country)

        self._find_element(
            locator=SearchLocators.COUNTRY_CHOOSE,
            driver=self.driver
        ).click()

    # --- Clicks ---
    def click_next(self):
        self._find_element(
            locator=SearchLocators.NEXT,
            driver=self.driver
        ).click()

    # --- Getters ---
    def get_country_phone_code_field(self):
        return self._find_element(
            locator=SearchLocators.COUNTRY_PHONE_CODE,
            driver=self.driver
        )

    def get_phone_input_field(self):
        return self._find_element(
            locator=SearchLocators.PHONE_INPUT_FIELD,
            driver=self.driver
        )

    def get_desktop_header_link(self):
        return self._find_element(
            locator=SearchLocators.DESKTOP_LINK_TEXT,
            driver=self.driver
        )

    def get_nacl_listener(self):
        return self._find_element(
            locator=SearchLocators.NACL_LISTENER,
            driver=self.driver
        )

    # --- External interfaces ---
    def find_element(self, locator: _Locator, time=10) -> WebElement:
        return self._find_element(locator, self.driver, time)

    def go_to_site(self):
        return self._go_to_site(self.link, self.driver)
